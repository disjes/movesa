﻿using System;
using System.Drawing;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.Design.Widget;
using CarouselView.FormsPlugin.Android;
using FFImageLoading;
using FFImageLoading.Forms.Platform;
using Color = Android.Graphics.Color;


namespace MovesaGarantias.Droid
{
    [Activity(Label = "MovesaGarantias", ScreenOrientation = ScreenOrientation.Portrait, Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Xamarin.Forms.Forms.SetFlags("FastRenderers_Experimental");

            CachedImageRenderer.Init(true);

            var config = new FFImageLoading.Config.Configuration()

            {

                VerboseLogging = false,

                VerbosePerformanceLogging = false,

                VerboseMemoryCacheLogging = false,

                VerboseLoadingCancelledLogging = false

            };

            ImageService.Instance.Initialize(config);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            Xamarin.FormsMaps.Init(this, bundle);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, bundle);
            CarouselViewRenderer.Init();
            //Push.SetSenderId("859183498268");
            LoadApplication(new App());

        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {

            global::ZXing.Net.Mobile.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}

