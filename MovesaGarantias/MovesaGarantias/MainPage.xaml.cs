﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarouselView.FormsPlugin.Abstractions;
using MovesaGarantias.ViewModels;
using MovesaGarantias.Views;
using Xamarin.Forms;

namespace MovesaGarantias
{
	public partial class MainPage : MasterDetailPage
	{
		public MainPage()
		{
			InitializeComponent();
		    
        }

	    public MainPage(Page detailpage)
	    {
	        // Set the master-part
	        Master = new MainPage();	        

	        // Set detail-part (with a navigation page)
	        Detail = new NavigationPage(detailpage);
	    }

        private void Button_Clicked(object sender, EventArgs e)
	    {
	        //Detail = new NavigationPage(new AddEmployee());
	        IsPresented = false;
	    }
	    private void Button_Clicked2(object sender, EventArgs e)
	    {
	        //Detail = new NavigationPage(new ListEmployee());
	        IsPresented = false;
	    }

	    private void Button_Clicked3(object sender, EventArgs e)
	    {
	        //Detail = new NavigationPage(new AboutUs());
	        IsPresented = false;
	    }
	    private void Button_Clickded4(object sender, EventArgs e)
	    {
	        //Detail = new NavigationPage(new ContactUs());
	        IsPresented = false;
	    }
        
	}
}
