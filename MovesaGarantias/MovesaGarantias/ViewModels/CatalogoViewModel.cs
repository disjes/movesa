﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MovesaGarantias.Models;

namespace MovesaGarantias.ViewModels
{
    public class CatalogoViewModel
    {
        public List<catalogo_productos> elements { get; set; }

        public CatalogoViewModel(List<catalogo_productos> elements)
        {
            this.elements = elements;
        }
    }
}
