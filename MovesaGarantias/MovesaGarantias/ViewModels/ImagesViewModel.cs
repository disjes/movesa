﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FFImageLoading.Cache;
using FFImageLoading.Forms;
using ModernHttpClient;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace MovesaGarantias.ViewModels
{
    class ImagesViewModel
    {
        //public ObservableCollection<Monkey> Monkeys { get; set; }
        //public ObservableCollection<Grouping<string, Monkey>> MonkeysGrouped { get; set; }

        public ObservableCollection<View> Zoos { get; set; }
        public string Title { get; set; }
        public string Info { get; set; }

        public ImagesViewModel()
        {
            //Monkeys = MonkeyHelper.Monkeys;
            //MonkeysGrouped = MonkeyHelper.MonkeysGrouped;
            Zoos = new ObservableCollection<View>
            {
                new CachedImage() { Source = "http://192.168.0.11:8555/Images/Slider/slider1.jpg", CacheType = CacheType.Memory, DownsampleToViewSize = true, Aspect = Aspect.Fill },
                new CachedImage() { Source = "http://192.168.0.11:8555/Images/Slider/slider2.jpg", CacheType = CacheType.Memory, DownsampleToViewSize = true, Aspect = Aspect.Fill },
                new CachedImage() { Source = "http://192.168.0.11:8555/Images/Slider/slider3.jpg", CacheType = CacheType.Memory, DownsampleToViewSize = true, Aspect = Aspect.Fill },  
                new CachedImage() { Source = "http://192.168.0.11:8555/Images/Slider/slider4.jpg", CacheType = CacheType.Memory, DownsampleToViewSize = true, Aspect = Aspect.Fill }       
            };
        }

        public ImagesViewModel(ObservableCollection<View> items)
        {
            //Monkeys = MonkeyHelper.Monkeys;
            //MonkeysGrouped = MonkeyHelper.MonkeysGrouped;
            Zoos = items;
        }

        public ImagesViewModel(ObservableCollection<View> items, string title, string info)
        {
            //Monkeys = MonkeyHelper.Monkeys;
            //MonkeysGrouped = MonkeyHelper.MonkeysGrouped;
            Zoos = items;
            Title = title;
            Info = info;
        }
    }
}
