﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.ViewModels
{
    public class SliderItem
    {
        public int id_slider { get; set; }
        public string imagen { get; set; }
        public string texto { get; set; }
        public int? orden { get; set; }
    }
}
