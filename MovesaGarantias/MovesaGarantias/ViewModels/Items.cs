﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.ViewModels
{
    public class Items
    {
        public DateTime End { get; set; }
        public DateTime Start { get; set; }
        public string @Abstract { get; set; }
        public string Title { get; set; }
        public string Presenter { get; set; }
        public string Biography { get; set; }    
        public string Image { get; set; }
        public string Avatar { get; set; }
        public string Room { get; set; }
    }
}
