﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovesaGarantias.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FullScreenView : ContentPage
	{
		public FullScreenView ()
		{
			InitializeComponent ();
		    BackgroundColor = Color.FromHex("#303030");
		}
	    public FullScreenView(string imageUrl)
	    {
	        InitializeComponent();	        
	    }
    }
}