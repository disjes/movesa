﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MovesaGarantias.Helpers;
using MovesaGarantias.Models;
using MovesaGarantias.ViewModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovesaGarantias.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FormListadoVehiculos : ContentPage
	{

	    ItemsViewModel viewModel;

        public FormListadoVehiculos()
		{
			InitializeComponent();		    
            string text = Settings.Phone;
            BindingContext = viewModel = new ItemsViewModel(text);
		    BackgroundColor = Color.FromHex("#303030");
		    
        }

        private async void ItemsListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	        var item = e.SelectedItem as Item;
	        var selected = (Item)((ListView) sender).SelectedItem;

	        if (item == null)
	            return;

	        var infoVehiculo = await GetItemsAsync(selected.Id);
	        var detailPage = new FormVehiculoInfo(selected.Id) {BindingContext = infoVehiculo};

	        var mdp = Application.Current.MainPage as MasterDetailPage;
            await mdp.Detail.Navigation.PushAsync(detailPage);
	        mdp.IsPresented = false;

        }

	    public async Task<InfoVehiculo> GetItemsAsync(string serieVehiculo)
	    {
	        HttpClient client = new HttpClient();
	        var RestUrl = Constants.Url + "GetInfoVehiculo/?serieVehiculo=" + serieVehiculo;
	        var uri = new Uri(RestUrl);
	        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
	        var response = await client.GetAsync(uri);
	        if (response.IsSuccessStatusCode)
	        {
	            var content = await response.Content.ReadAsStringAsync();
	            var items = JsonConvert.DeserializeObject<InfoVehiculo>(content);
	            return items;
	        }

	        return null;
	    }
        
        protected override void OnAppearing()
	    {
	        base.OnAppearing();	       
            if (viewModel.Items.Count == 0)
	            viewModel.LoadItemsCommand.Execute(null);
	    }

    }
}