﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using MovesaGarantias.Models;
using MovesaGarantias.ViewModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovesaGarantias.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CatalogoProductos : ContentPage
	{
		public CatalogoProductos ()
		{
			InitializeComponent ();
		    //preview.GestureRecognizers.Add(
		    //    new TapGestureRecognizer()
		    //    {
		    //        Command = new Command(GoFullScreen)
		    //    });
        }

	    private async void GoFullScreen(catalogo_productos selectedItem)
	    {
	        var items = GetBindingContext(selectedItem);
	        var mdp = Application.Current.MainPage as MasterDetailPage;
            var page = new FullScreenView();
	        page.BindingContext = new ImagesViewModel(items, selectedItem.nombre_producto ?? "Titulo Prueba", selectedItem.resenia_producto ?? "Informacion de Prueba");
	        await mdp.Detail.Navigation.PushAsync(page);
	        mdp.IsPresented = false;
        }
	    private ObservableCollection<View> GetBindingContext(catalogo_productos item)
	    {
	        var collection = new ObservableCollection<View>();

            if(!string.IsNullOrEmpty(item.imagen)) collection.Add(new CachedImage() { Source = item.imagen, DownsampleToViewSize = true, Aspect = Aspect.AspectFit });
	        if (!string.IsNullOrEmpty(item.imagen2)) collection.Add(new CachedImage() { Source = item.imagen2, DownsampleToViewSize = true, Aspect = Aspect.AspectFit });
	        if (!string.IsNullOrEmpty(item.imagen3)) collection.Add(new CachedImage() { Source = item.imagen3, DownsampleToViewSize = true, Aspect = Aspect.AspectFit });
	       
	        return collection;
	    }

        private void ListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
	    {
	       
	        var selected = (catalogo_productos)((ListView)sender).SelectedItem;

	        if (selected == null)
	            return;

	       GoFullScreen(selected);
        }
	}
}