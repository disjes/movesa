﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MovesaGarantias.ViewModels;
using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Position = Xamarin.Forms.Maps.Position;

namespace MovesaGarantias.Views
{

    public class MapPoint
    {

        public Position Position { get; set; }
        public string Label { get; set; }
        public string Address { get; set; }

        public MapPoint(Position position, string label, string address)
        {
            this.Position = position;
            this.Label = label;
            this.Address = address;
        }


    }

	public class MapPage : ContentPage
	{

	    private Plugin.Geolocator.Abstractions.Position asyncPosition;
        
		public MapPage ()
		{
		    this.Title = "Nuestros Talleres";
		}

	    protected override async void OnAppearing()
	    {
	        if (await CheckLocationPermission())
	        {
	            var locator = CrossGeolocator.Current;
	            Plugin.Geolocator.Abstractions.Position position = null;

	            try
	            {
	                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(2));

	                var map = new Map(
	                    MapSpan.FromCenterAndRadius(
	                        new Position(position.Latitude, position.Longitude), Distance.FromMiles(3)))
	                {
	                    IsShowingUser = true,
	                    HeightRequest = 100,
	                    WidthRequest = 960,
	                    VerticalOptions = LayoutOptions.FillAndExpand
	                };

	                MarkTalleres(map);

	                var stack = new StackLayout {Spacing = 0};
	                stack.Children.Add(map);
	                Content = stack;
	            }
	            catch (Exception ex)
	            {
	                await DisplayAlert("Gps Apagado", "Por favor encienda el Gps de su telefono para acceder.", "OK");
	                if (Device.RuntimePlatform == global::Xamarin.Forms.Device.iOS)
	                {
	                    // For iOS 8 and 9, we can navigate automatically to the settings
	                    //NSUrl url = new NSUrl(UIKit.UIApplication.OpenSettingsUrlString);
	                    //bool result = UIApplication.SharedApplication.OpenUrl(url);

                    }
                    await Navigation.PopAsync();
	            }
	        }
	    }

	    private async void MarkTalleres(Map map)
	    {

	        List<MapPoint> talleres = await GetTalleres();
	            
	        //new List<Taller>
	        //{
	        //    new Taller(new Position(15.533230, -87.976615), "Taller 1", "Ocotillo"),
	        //    new Taller(new Position(15.526139, -87.967786), "Taller 2", "El Carmen"),
	        //    new Taller(new Position(15.526696, -87.989273), "Taller 3", "San Martin"),
	        //    new Taller(new Position(15.525297, -87.972858), "Taller 4", "Campisa"),
	        //    new Taller(new Position(15.519588, -87.976759), "Taller 5", "Salamanca")	            
	        //};

	        talleres.ForEach(x =>
            {
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = x.Position,
                    Label = x.Label,
                    Address = x.Address
                };
                map.Pins.Add(pin);
            });
        }

	    private async Task<bool> CheckLocationPermission()
	    {
	        try
	        {
	            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
	            if (status != PermissionStatus.Granted)
	            {
	                if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
	                {
	                    await DisplayAlert("Se necesita permisos de GPS", "Se necesita acceder a su Gps para indicarle los talleres mas cercanos", "OK");
	                }

	                var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
	                //Best practice to always check that the key exists
	                if (results.ContainsKey(Permission.Location))
	                    status = results[Permission.Location];
	            }

	            if (status == PermissionStatus.Granted)
	            {
	                var results = await CrossGeolocator.Current.GetPositionAsync(TimeSpan.FromSeconds(2));
	                return true;
	            }
	            else if (status != PermissionStatus.Unknown)
	            {
	                await DisplayAlert("Ubicacion denegada", "No se puede continuar, intente de nuevo", "OK");
	            }
	        }
	        catch (Exception ex)
	        {

	            return false;
	        }

	        return false;
	    }

	    private async Task<List<MapPoint>> GetTalleres()
	    {
	        List<MapPoint> talleres = new List<MapPoint>();
	        HttpClient client = new HttpClient();
	        var RestUrl = Constants.Url + "GetTalleres";
            var uri = new Uri(RestUrl);
	        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
	        var response = await client.GetAsync(uri);
	        if (response.IsSuccessStatusCode)
	        {
	            var content = await response.Content.ReadAsStringAsync();
	            talleres = GetTalleresAsMapPointsList(JsonConvert.DeserializeObject<List<talleres>>(content));	            
	        }
	        return talleres;
        }

	    private List<MapPoint> GetTalleresAsMapPointsList(List<talleres> items)
	    {
	        var collection = new List<MapPoint>();
	        items.ForEach(x =>
	        {
	            double lat = Convert.ToDouble(x.lat);
                double @long = Convert.ToDouble(x.@long);
	            collection.Add(new MapPoint(new Position(lat, @long), x.descripcion_taller, ""));	            
	        });
	        return collection;
	    }

        /*	    private async Task<MapPage> InitializeAsync()
                {
                    var locator = CrossGeolocator.Current;
                    asyncPosition = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));
                    return this;
                }

                public static Task<MapPage> CreateAsync()
                {
                    var ret = new MapPage();
                    return ret.InitializeAsync();
                }

                public static async System.Threading.Tasks.Task UseMapPageAsync()
                {
                    MapPage mapPage = await MapPage.CreateAsync();
                    var map = new Map(
                        MapSpan.FromCenterAndRadius(
                            new Position(mapPage.asyncPosition.Latitude, mapPage.asyncPosition.Longitude),
                            Distance.FromMiles(0.3)))
                    {
                        IsShowingUser = true,
                        HeightRequest = 100,
                        WidthRequest = 960,
                        VerticalOptions = LayoutOptions.FillAndExpand,
                        MapType = MapType.Satellite
                    };

                    var stack = new StackLayout { Spacing = 0 };
                    stack.Children.Add(map);
                    mapPage.Content = stack;
                }*/

    }
}