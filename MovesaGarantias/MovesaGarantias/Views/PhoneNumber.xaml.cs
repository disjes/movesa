﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MovesaGarantias.Helpers;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovesaGarantias.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PhoneNumber : ContentPage
	{
		public PhoneNumber ()
		{
			InitializeComponent ();
		    MyEntry.Unfocus();
		    MyEntry.Text = "";
		    MyEntry.Focus();
		    BackgroundColor = Color.FromHex("#303030");
        }

	    protected override async void OnAppearing()
	    {
	        if (Settings.IsPhoneSet)
	        {
	            var mdp = Application.Current.MainPage as MasterDetailPage;
	            await mdp.Detail.Navigation.PopToRootAsync();
	            mdp.IsPresented = false;
	        }
        }

	    private async void SaveButton_OnClicked(object sender, EventArgs e)
	    {
	        if (!string.IsNullOrWhiteSpace(MyEntry.Text))
	        {
	            Settings.Phone = MyEntry.Text.Trim();
	            var mdp = Application.Current.MainPage as MasterDetailPage;
	            await mdp.Detail.Navigation.PushAsync(new FormListadoVehiculos());
	            mdp.IsPresented = false;
	        }
        }
	}
}