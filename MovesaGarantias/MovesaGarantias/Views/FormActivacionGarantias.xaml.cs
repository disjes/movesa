﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace MovesaGarantias.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FormActivacionGarantias : ContentPage
	{
		public FormActivacionGarantias ()
		{
			InitializeComponent ();
		    Scanner.GestureRecognizers.Add(
		        new TapGestureRecognizer()
		        {
		            Command = new Command(Scan)
		        });
        }

	    private void Button_OnClicked(object sender, EventArgs e)
	    {
	        Scan();
        }

	    private void Scan()
	    {
            var scanPage = new ZXingScannerPage();
	        Navigation.PushAsync(scanPage);
	        scanPage.OnScanResult += (reslut) =>
	        {
	            Device.BeginInvokeOnMainThread(async () =>
	            {
	                Navigation.PopAsync();
	                TxtNumSerie.Text = reslut.Text;
	            });

	        };
	    }

	    private async void ButtonActivar_OnClicked(object sender, EventArgs e)
	    {	        
	        string numeroserie = TxtNumSerie.Text;
	        string nombrecliente = TxtNombreCliente.Text;
	        string telefono = TxtTelefono.Text;
	        HttpClient client = new HttpClient();
	        var RestUrl = Constants.Url + "ActivarGarantia?numeroserie={0}&nombrecliente={1}&telefono={2}";
	        var uri = new Uri(string.Format(RestUrl, numeroserie, nombrecliente, telefono));	           
	        var response = await client.GetAsync(uri);
	        if (response.IsSuccessStatusCode)
	        {
	            var content = await response.Content.ReadAsStringAsync();
	            await DisplayAlert("Movesa", content, "Ok");
	            var mdp = Application.Current.MainPage as MasterDetailPage;
	            await mdp.Detail.Navigation.PopAsync();
	            mdp.IsPresented = false;                
	        }
        }
	}
}