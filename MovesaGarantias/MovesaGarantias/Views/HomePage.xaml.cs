﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CarouselView.FormsPlugin.Abstractions;
using FFImageLoading.Forms;
using ModernHttpClient;
using MovesaGarantias.Helpers;
using MovesaGarantias.Models;
using MovesaGarantias.ViewModels;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MovesaGarantias.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();                       
            SetCommandsToLayoutButtons();
            var context = new ImagesViewModel();
		    BindingContext = context;
		    int SlidePosition = 0;
		    int count = new ImagesViewModel().Zoos.Count;
		    Device.StartTimer(TimeSpan.FromSeconds(2), () =>
		    {
		        SlidePosition++;
		        if (SlidePosition == count)
		        {
		            SlidePosition = 0;
		        }
		        Carousel.Position = SlidePosition;
		        return true;
		    });

        }

	    protected override async void OnAppearing()
	    {
            //ObservableCollection<View> items = await GetImagesAndFillSlider();
	        
        }

	    public async Task<string> GetUrl(string numImage)
	    {
	        HttpClient client = new HttpClient();
	        string urlSlider = Constants.Url + "GetSlider?id=";
            var RestUrl = urlSlider + numImage;
	        var uri = new Uri(RestUrl);
	        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
	        var response = await client.GetAsync(uri);
	        if (response.IsSuccessStatusCode)
	        {
	            var content = await response.Content.ReadAsStringAsync();
	            string url = JsonConvert.DeserializeObject<string>(content);
	            return url;
	        }

	        return "";
	    }

        private async Task<ObservableCollection<View>> GetImagesAndFillSlider()
	    {
	        ObservableCollection<View> items = new ObservableCollection<View>();
	        HttpClient client = new HttpClient();
	        var RestUrl = Constants.Url + "GetSlider";
            var uri = new Uri(RestUrl);
	        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetAsync(uri);
	        if (response.IsSuccessStatusCode)
	        {
	            var content = await response.Content.ReadAsStringAsync();
	            items = GetBindingContext(JsonConvert.DeserializeObject<List<SliderItem>>(content));
	            return items;
	        }

	        return null;
	    }
        
        private ObservableCollection<View> GetBindingContext(List<SliderItem> items)
	    {
	        var collection = new ObservableCollection<View>();
            items.ForEach(x =>
            {
               collection.Add(new CachedImage()
               {
                   Source = x.imagen,
                   DownsampleToViewSize = true,
                   Aspect = Aspect.Fill
               });
            });
	        return collection;
	    }

	    private void SetCommandsToLayoutButtons()
	    {
	        ButtonActivacion.GestureRecognizers.Add(
	            new TapGestureRecognizer()
	            {
	                Command = new Command(ButtonActivacion_OnClicked)
	            });

	        ButtonServicios.GestureRecognizers.Add(
	            new TapGestureRecognizer()
	            {
	                Command = new Command(ButtonServicios_Clicked)
	            });

	        ButtonTalleres.GestureRecognizers.Add(
	            new TapGestureRecognizer()
	            {
	                Command = new Command(Button_OnClicked)
	            });

	        ButtonDistribuidores.GestureRecognizers.Add(
	            new TapGestureRecognizer()
	            {
	                Command = new Command(ButtonDistribuidores_OnClicked)
	            });

	        ButtonModelos.GestureRecognizers.Add(
	            new TapGestureRecognizer()
	            {
	                Command = new Command(ButtonCatalogo_OnClicked)
	            });

	        ButtonAccesorios.GestureRecognizers.Add(
	            new TapGestureRecognizer()
	            {
	                Command = new Command(ButtonCatalogoAccesorios_OnClicked)
	            });
        }

	    private void ButtonServicios_Clicked()
	    {            
	        var mdp = Application.Current.MainPage as MasterDetailPage;
            if (Settings.IsPhoneSet) mdp.Detail.Navigation.PushAsync(new FormListadoVehiculos());
	        else mdp.Detail.Navigation.PushAsync(new PhoneNumber());
	        mdp.IsPresented = false;
        }

	    private async void ButtonCatalogoAccesorios_OnClicked()
	    {
	        var mdp = Application.Current.MainPage as MasterDetailPage;
	        var pageCatalogo = new CatalogoProductos();
	        var items = await GetCatalogo("GetAccesorios");
	        pageCatalogo.BindingContext = new CatalogoViewModel(items);
	        await mdp.Detail.Navigation.PushAsync(pageCatalogo);
	        mdp.IsPresented = false;

         //   var mdp = Application.Current.MainPage as MasterDetailPage;
	        //mdp.Detail.Navigation.PushAsync(new CatalogAccesorios());
	        //mdp.IsPresented = false;
        }

	    private void ButtonActivacion_OnClicked()
	    {
	        var mdp = Application.Current.MainPage as MasterDetailPage;
	        mdp.Detail.Navigation.PushAsync(new FormActivacionGarantias() );           
            mdp.IsPresented = false;
        }

	    private void Button_OnClicked()
	    {
	        var mdp = Application.Current.MainPage as MasterDetailPage;
	        mdp.Detail.Navigation.PushAsync(new MapPage());
            mdp.IsPresented = false;
        }

	    private void ButtonDistribuidores_OnClicked()
	    {
	        var mdp = Application.Current.MainPage as MasterDetailPage;
	        mdp.Detail.Navigation.PushAsync(new DistribuidoresPage());
	        mdp.IsPresented = false;
        }


        private void Carousel_OnPositionSelected(object sender, PositionSelectedEventArgs e)
	    {

	    }

	    private async void ButtonCatalogo_OnClicked()
	    {
	        var mdp = Application.Current.MainPage as MasterDetailPage;
            var pageCatalogo = new CatalogoProductos();
	        var items = await GetCatalogo("GetModelos");
	        pageCatalogo.BindingContext = new CatalogoViewModel(items);
	        await mdp.Detail.Navigation.PushAsync(pageCatalogo);
            mdp.IsPresented = false;
        }

	    public async Task<List<catalogo_productos>> GetCatalogo(string method)
	    {
	        HttpClient client = new HttpClient();
	        var RestUrl = Constants.Url + method;
	        var uri = new Uri(RestUrl);
	        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
	        var response = await client.GetAsync(uri);
	        if (response.IsSuccessStatusCode)
	        {
	            var content = await response.Content.ReadAsStringAsync();
	            var items = JsonConvert.DeserializeObject<List<catalogo_productos>>(content);
	            return items;
	        }

	        return null;
	    }

    }
}