﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using MovesaGarantias.Views;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace MovesaGarantias
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

			MainPage = new MainPage();
		    //OneSignal.Current.StartInit("29ec9e10-9265-4985-a9e3-f90294cd7f15")
		    //    .EndInit();

        }

	    public static Page GetMainPage()
	    {
	        return new NavigationPage(new MainPage());
	    }

        protected override async void OnStart ()
		{

        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
