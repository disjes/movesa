﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MovesaGarantias.Models;
using MovesaGarantias.ViewModels;
using Newtonsoft.Json;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(MovesaGarantias.Services.MockDataStore))]
namespace MovesaGarantias.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        List<Item> items;
        public string phone;

        public MockDataStore(string phone)
        {
            var currentDate = DateTime.Now.ToString("dd/MM/yyyy");
            var description = "This is an item description for item";
            this.phone = phone;

            items = new List<Item>();
            var mockItems = new List<Item>
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "Item Title #1".ToUpper(), Description = description, PublishDate = currentDate, BoxColor = "#03a9f4" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Item Title #2".ToUpper(), Description = description, PublishDate = currentDate, BoxColor = "#e91e63" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Item Title #3".ToUpper(), Description = description, PublishDate = currentDate, BoxColor = "#673ab7" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Item Title #4".ToUpper(), Description = description, PublishDate = currentDate, BoxColor = "#3f51b5" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Item Title #5".ToUpper(), Description = description, PublishDate = currentDate, BoxColor = "#f44336" },
            };

            foreach (var item in mockItems)
                items.Add(item);
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var _item = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(_item);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var _item = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(_item);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<List<Item>> GetItemsAsync(bool forceRefresh = false)
        {            
            HttpClient client = new HttpClient();
            var RestUrl = Constants.Url + "GetVehiculos/?phoneNumber=" + phone;
            var uri = new Uri(RestUrl);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                var items = GetModelItems(JsonConvert.DeserializeObject<List<vehiculo>>(content));
                return items;
            }

            return null;
        }

        private List<Item> GetModelItems(List<vehiculo> items)
        {
            var collection = new List<Item>();
            items.ForEach(x =>
            {
                collection.Add(new Item()
                {
                    Id = x.serieVehiculo,
                    Text = "Marca:  " + x.marca + "\n" +
                           "Modelo: " + x.modelo + "\n" +
                           "Serie:  " + x.serieVehiculo,
                    PublishDate = DateTime.Today.ToString(),
                    BoxColor = "#000000"
                });
            });
            return collection;
        }
    }
}
