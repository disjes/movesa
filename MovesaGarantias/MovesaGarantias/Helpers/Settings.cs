﻿// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace MovesaGarantias.Helpers
{
/// <summary>
/// This is the Settings static class that can be used in your Core solution or in any
/// of your client applications. All settings are laid out the same exact way with getters
/// and setters. 
/// </summary>
public static class Settings
{
    private static ISettings AppSettings => CrossSettings.Current;

    #region Setting Constants

    //private const string PhoneNumber = "phone";
    //private static readonly string SettingsDefault = string.Empty;

    #endregion

    public static bool IsPhoneSet => AppSettings.Contains(nameof(Phone));

    public static string Phone
    {
        get => AppSettings.GetValueOrDefault<string>(nameof(Phone), string.Empty);
        set => AppSettings.AddOrUpdateValue<string>(nameof(Phone), value);
    }

}
}
