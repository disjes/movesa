﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.Models
{
    public class certificados_garantia
    {
        public long idCG { get; set; }
        public string serieVehiculo { get; set; }
        public Nullable<System.DateTime> fechaArticavion { get; set; }
        public Nullable<bool> estadoGarantia { get; set; }
        public string metodoActivacion { get; set; }
        public string agenciaCompra { get; set; }
        public Nullable<System.DateTime> fechaCompra { get; set; }
        public string numeroFactura { get; set; }
        public string vendedor { get; set; }
        public Nullable<bool> contrato { get; set; }
        public string comentario { get; set; }
        public Nullable<long> idCliente { get; set; }
        public Nullable<long> idVehiculo { get; set; }
        public Nullable<bool> estadoFactudado { get; set; }
        public Nullable<System.DateTime> fechaCierre { get; set; }
        public Nullable<int> idUsuario { get; set; }
    }
}
