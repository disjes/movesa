﻿using System;
using System.Collections.Generic;
using System.Text;
using ZXing.Common;

namespace MovesaGarantias.Models
{
    public class InfoVehiculo
    {
        public clientes Cliente { get; set; }
        public vehiculo Vehiculo { get; set; }
        public certificados_garantia CertificadoGarantia { get; set; }
        public List<View_OrdenesServcioProgramadasVehiculo> OrdenesServicio { get; set; }
        public List<View_ServiciosGarantia_Vehiculo> ServiciosGarantia { get; set; }

        public string NombreCompletoCliente => Cliente.primerNombre?.Trim() + " " +
                                               Cliente.segundoNombre?.Trim() + " " +
                                               Cliente.primerApellido?.Trim() + " " +
                                               Cliente.segundoApellido?.Trim();

        public EncodingOptions Options => new EncodingOptions()
        {
            Width = 200,
            Height = 200,
            Margin = 1,
        };
    }
}
