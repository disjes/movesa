﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.Models
{
    public class View_ServiciosGarantia_Vehiculo
    {
        public string cliPNombre { get; set; }
        public string cliSNombre { get; set; }
        public string cliIdentidad { get; set; }
        public string cliPApellido { get; set; }
        public string cliSApellido { get; set; }
        public string cliCelular { get; set; }
        public string cliTelefono { get; set; }
        public string cliCorreo { get; set; }
        public string cliDepartamento { get; set; }
        public string cliMunicipio { get; set; }
        public string cliCiudadPueblo { get; set; }
        public string cliDireccion { get; set; }
        public Nullable<System.DateTime> ssgFechaCreacion { get; set; }
        public string vhSerie { get; set; }
        public string vhMarca { get; set; }
        public string vhModelo { get; set; }
        public string vhDescripcion { get; set; }
        public string vhChasis { get; set; }
        public Nullable<int> vhYear { get; set; }
        public string vhColor { get; set; }
        public string vhMotor { get; set; }
        public string tllNombre { get; set; }
        public string tllDepartamento { get; set; }
        public string tllMunicipio { get; set; }
        public string tllDireccion { get; set; }
        public string ssgDescripcionTrabajo { get; set; }
        public string ssgObservaciones { get; set; }
        public string ssgDiagnostico { get; set; }
        public string osRosolucionProblema { get; set; }
        public string osComentarioCliente { get; set; }
        public string osCategoriaOS { get; set; }
        public string usrPNombre { get; set; }
        public string usrSNombre { get; set; }
        public string usrPApellido { get; set; }
        public string usrSApellido { get; set; }
        public string usrIdentidad { get; set; }
        public string usrCelular { get; set; }
        public string ssgEstado { get; set; }
        public Nullable<bool> ssgAprobado { get; set; }
    }
}
