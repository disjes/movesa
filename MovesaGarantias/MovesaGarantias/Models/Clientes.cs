﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.Models
{
    public class clientes
    {
        public long idCliente { get; set; }
        public string identidad { get; set; }
        public string primerNombre { get; set; }
        public string segundoNombre { get; set; }
        public string primerApellido { get; set; }
        public string segundoApellido { get; set; }
        public string celular { get; set; }
        public string telefono { get; set; }
        public Nullable<System.DateTime> fechaNacimiento { get; set; }
        public string correoElectronico { get; set; }
        public string departamento { get; set; }
        public string municipio { get; set; }
        public string ciudadPueblo { get; set; }
        public string direccion { get; set; }
    }
}
