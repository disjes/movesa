﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.Models
{
    public class View_OrdenesServcioProgramadasVehiculo
    {
        public long idOS { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public string serieVehiculo { get; set; }
        public Nullable<int> kms { get; set; }
        public string comentariosCliente { get; set; }
        public string categoriaOrdenServ { get; set; }
        public string diagnostico { get; set; }
        public string resoluciondelProblema { get; set; }
        public Nullable<bool> abierta { get; set; }
        public string descripcion { get; set; }
        public Nullable<int> srKms { get; set; }
        public Nullable<int> ruedas { get; set; }
        public string trabajoRealizar { get; set; }
        public string identidad { get; set; }
        public string primerNombre { get; set; }
        public string segundoNombre { get; set; }
        public string primerApellido { get; set; }
        public string segundoApellido { get; set; }
        public string celular { get; set; }
        public string telefono { get; set; }
        public string correoElectronico { get; set; }
        public string departamento { get; set; }
        public string municipio { get; set; }
        public string ciudadPueblo { get; set; }
        public string direccion { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string color { get; set; }
        public string motor { get; set; }
        public string chasis { get; set; }
        public Nullable<int> year { get; set; }
        public string vhDescripcion { get; set; }
        public string usrPNombre { get; set; }
        public string usrSNombre { get; set; }
        public string userPApellido { get; set; }
        public string usrSApellido { get; set; }
        public string usrIdentidad { get; set; }
        public string usrCelular { get; set; }
    }
}
