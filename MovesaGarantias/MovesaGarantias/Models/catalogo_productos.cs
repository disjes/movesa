﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.Models
{
    public class catalogo_productos
    {
        public int id_producto { get; set; }
        public Nullable<int> id_categoria { get; set; }
        public string codigo { get; set; }
        public string nombre_producto { get; set; }
        public string descripcion_producto { get; set; }
        public string imagen { get; set; }
        public string imagen2 { get; set; }
        public string imagen3 { get; set; }
        public string resenia_producto { get; set; }
        public Nullable<decimal> precio { get; set; }
        public Nullable<bool> estado { get; set; }
        public Nullable<int> orden { get; set; }

        public virtual categorias categorias { get; set; }
    }
}
