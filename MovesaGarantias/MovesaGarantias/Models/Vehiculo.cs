﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovesaGarantias.Models
{
    public class vehiculo
    {
        public long idVehiculo { get; set; }
        public string serieVehiculo { get; set; }
        public Nullable<int> cantidadRuedas { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public string descripcion { get; set; }
        public string chasis { get; set; }
        public Nullable<int> year { get; set; }
        public string color { get; set; }
        public string motor { get; set; }
    }
}
